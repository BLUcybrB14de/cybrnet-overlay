# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Eselect module for managing LLVM installations"
HOMEPAGE="https://github.com/Infrasonics/eselect-llvm"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="app-admin/eselect"

S="${WORKDIR}"

src_install() {
    insinto /usr/share/eselect/modules
    doins "${FILESDIR}/llvm.eselect"
}
