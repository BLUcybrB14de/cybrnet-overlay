# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Daddy is here for you on the command line ~ heart"
HOMEPAGE="https://github.com/sudofox/shell-mommy"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="sys-apps/coreutils"

S="${WORKDIR}"

src_install() {
    dobin "${FILESDIR}/shell-daddy"
	elog ""
	elog "This is a soft fork of shell-mommy"
	elog ""
	elog "you can source the shell-daddy script in your current shell or add it to your ~/.bashrc"
	elog "file to have it available every time you open a new terminal. . /path/to/shell-daddy"
	elog " If you'd like it to always show a message after each command, you can simply"
	elog "export PROMPT_COMMAND='daddy \\$\\(exit \$?\\); $PROMPT_COMMAND' "
	elog "visit https://github.com/sudofox/shell-mommy for documentation"
	elog "also see /usr/bin/shell-daddy - for changed variables"
	elog ""
	elog "... That's a good kiddo~"
}


