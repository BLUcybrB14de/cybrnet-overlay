# Copyright 2024 The Coding Liberation Front - for a better future

EAPI=8

DESCRIPTION="Copyleft + Permissive License, that strikes a balance between permissive and non-permissive licenses"
HOMEPAGE="https://codeberg.org/Coding-Liberation.net/COPL"
SRC_URI=""

LICENSE="COPL-0.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="sys-apps/coreutils"

S="${WORKDIR}

src_install() {
    dodir /usr/share/doc/COPL-0.1/
    insinto /usr/share/doc/COPL-0.1
    doins "${FILESDIR}/COPL-0.1/LICENSE" 
    fperms 0644 /usr/share/doc/COPL-0.1/LICENSE"
    elog ""
    elog "LICENSE is installed at /usr/share/doc/COPL-0.1/LICENSE"
    elog "xo."
}

