# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit git-r3

DESCRIPTION="A utility for managing BetterDiscord on Linux"
HOMEPAGE="https://github.com/bb010g/betterdiscordctl"
EGIT_REPO_URI="https://github.com/bb010g/${PN}.git"

LICENSE=""
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="net-misc/curl"
DEPEND="${RDEPEND}"
BDEPEND=""

src_prepare(){
    default
    sed -i "s/DISABLE_SELF_UPGRADE=/DISABLE_SELF_UPGRADE=1/g" betterdiscordctl
}

src_install(){
    exeinto "/usr/bin"
    doexe "betterdiscordctl"
}