# Terms of Contribution

### The Coding Liberation Front - "Code of Conduct"

<p> This is a fork of <a href="https://www.debian.org/code_of_conduct">code_of_conduct</a>. </p>

- Be respectful

  In a project the size of this $GIT_PROJECT, inevitably there will be people with whom you may disagree, or find it difficult to cooperate. Accept that, but even so, remain respectful. Disagreement is no excuse for poor behaviour or personal attacks, and a community in which people feel threatened is not a healthy community.

  Assume good faith

  $GIT_PROJECT Contributors have many ways of reaching our common goal of a free $GIT_PROJECT which may differ from your ways. Assume that other people are working towards this goal.

  Note that many of our Contributors are not native English speakers or may have different cultural backgrounds.

- Be collaborative

  This $GIT_PROJECT is a large and complex project; there is always more to learn within the $GIT_PROJECT. It's good to ask for help when you need it. Similarly, offers for help should be seen in the context of our shared goal of improving $GIT_PROJECT.

  When you make something for the benefit of the project, be willing to explain to others how it works, so that they can build on your work to make it even better.

- Try to be concise

  Keep in mind that what you write once will be read by hundreds of persons. Writing a short email means people can understand the conversation as efficiently as possible. When a long explanation is necessary, consider adding a summary.

  Try to bring new arguments to a conversation so that each mail adds something unique to the thread, keeping in mind that the rest of the thread still contains the other messages with arguments that have already been made.

  Try to stay on topic, especially in discussions that are already fairly large.

- Be open

  Most ways of communication used within $GIT_PROJECT allow for public and private communication. As per paragraph three of the social contract, you should preferably use public methods of communication for $GIT_PROJECT-related messages, unless posting something sensitive.

  This applies to messages for help or $GIT_PROJECT-related support, too; not only is a public support request much more likely to result in an answer to your question, it also makes sure that any inadvertent mistakes made by people answering your question will be more easily detected and corrected.

- In case of problems

  While this code of conduct should be adhered to by participants, we recognize that sometimes people may have a bad day, or be unaware of some of the guidelines in this code of conduct. When that happens, you may reply to them and point out this code of conduct. Such messages may be in public or in private, whatever is most appropriate. However, regardless of whether the message is public or not, it should still adhere to the relevant parts of this code of conduct; in particular, it should not be abusive or disrespectful. Assume good faith; it is more likely that participants are unaware of their bad behaviour than that they intentionally try to degrade the quality of the discussion.

  Serious or persistent offenders will be temporarily or permanently banned from communicating through $GIT_PROJECT's systems. Complaints should be made (in private) to the administrators of the $GIT_PROJECT communication forum in question. To find contact information for these administrators, please see the page on $GIT_PROJECT's organizational structure.

Reporting issues

Contact the $GIT_PROJECT Community Team: (Under # Contact)

Further reading

Some of the links in this section do not refer to documents that are part of this code of conduct, nor are they authoritative within $GIT_PROJECT. However, they all do contain useful information on how to conduct oneself on our communication channels.

- [$GIT_PROJECT has a diversity statement.](https://www.debian.org/intro/diversity)
- [The $GIT_PROJECT Community Guidelines by Enrico Zini contain some advice on how to communicate effectively.](https://people.debian.org/~enrico/dcg)
- [The Mailing list code of conduct is useful for advice specific to $GIT_PROJECT mailing lists.](https://www.debian.org/MailingLists/#codeofconduct)
- [The Community Team have written some extra guidance on how to interpret the Code of Conduct.](https://www.debian.org/codeofconduct/interpretation/)

Updates to this code of conduct should follow after # Contact, and creating [an issue](https://docs.codeberg.org/getting-started/issue-tracking-basics/).
<hr>

## The Coding Liberation Front - "Wolf Pack Philosophy"

<p> This is a fork of <a href="https://www.funtoo.org/Wolf_Pack_Philosophy">Wolf_Pack_Philosophy</a>.</p><br>

<img alt="Wolf.png" src="https://www.funtoo.org/images/thumb/c/c8/Wolf.png/500px-Wolf.png" decoding="async" srcset="/images/thumb/c/c8/Wolf.png/750px-Wolf.png 1.5x, /images/thumb/c/c8/Wolf.png/1000px-Wolf.png 2x" width="500" height="375">

<br>

The philosophy of this $GIT_PROJECT community, which is focused on $GIT_PROJECT is based on the analogy of a wolf pack. The following are the pillars of the Wolf Pack Philosophy that guide the community:

- Authenticity: The community emphasizes the importance of staying connected to the open source "wilderness" and avoiding pre-packaged solutions that disconnect users from the raw creative energy of the open source ecosystem.

- Mindfulness of the Pack: The community supports and looks out for each other, tackling challenges as a community and actively trying to find common paths to cultivate together.

- Interconnectedness: The community's connection to each other is wired into their consciousness and awareness. They support each other instinctively and coordinate their actions as a logical entity.

- Selectivity: The community carefully evaluates and selects the solutions that reflect their deeply held values, avoiding solutions that do not align with their philosophy.

- Hunting: The community is not satisfied with suboptimal solutions and is committed to hunting for the best technology, even if it requires investing time and raw creative energy.

- Territoriality: The community takes ownership of their territory while being aware of its limits and not biting off more than they can chew.

- Ownership of Mistakes: The community expects members to take responsibility for their mistakes and be transparent and engaged in helping to clean them up.

- Expressiveness: The community encourages members to express their personal desires and needs to enrich the pack's understanding and support of each other.

The technical decisions made by the community, such as the creation of an official Support Matrix, easier install process, pre-built kernels, and ready-to-use desktop environments, align with the Wolf Pack Philosophy. The decisions are made to support the community's values of authenticity, mindfulness of the pack, selectivity, hunting, territoriality, and ownership of mistakes. The community also uses automation technology to free up time for more useful work, such as developing new technologies.

Did You Notice?
If you read this far, please notice that the wolf illustration has five legs. Behold, the mighty five-legged wolf!
<br>

<p> Additonal terms may apply on the project README.md </p> 

## Contact

 <a href="https://proton.me/"><img src="https://codeberg.org/blucybrb14de/dotfiles/raw/branch/gentoo-hyprland/images/protonmail.webp"  style="vertical-align: middle" height="20" width="20"></a> blucybrb14de@coding-liberation.org
