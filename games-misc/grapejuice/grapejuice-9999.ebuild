
# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{8..11} pypy3 )
inherit distutils-r1 git-r3

DESCRIPTION=""
HOMEPAGE=""
EGIT_REPO_URI="https://gitlab.com/brinkervii/grapejuice.git"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS=""

DEPEND="
	${PYTHON_DEPS}
	dev-python/psutil
	dev-python/pygobject
	dev-python/packaging
	dev-python/requests
	dev-python/unidecode
	dev-python/click
	dev-python/pydantic
	sys-devel/gettext
	x11-libs/cairo
	x11-libs/gtk+
	dev-libs/gobject-introspection
	dev-util/desktop-file-utils
	x11-misc/xdg-utils
	x11-misc/xdg-user-dirs
	dev-util/gtk-update-icon-cache
	x11-misc/shared-mime-info
	x11-apps/mesa-progs"
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}/${P}
