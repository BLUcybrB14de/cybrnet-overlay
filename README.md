<img style="margin-right: 30px;" src="https://codeberg.org/repo-avatars/68f4b98f58cf72d55cff7ae2ff05841b638d606b6909f9688c238aeebdcd8f6c" height="100" align="left">

# cybrnet-overlay <a href="https://codeberg.org/blucybrb14de/cybrnet-overlay/"><img src="https://custom-icon-badges.demolab.com/badge/hosted%20on-codeberg-4793CC.svg?logo=codeberg&logoColor=white" alt="Codeberg badge"></a>

<br>

### Obscure Software - intended for use with [Gentoo GNU/Linux](https://codeberg.org/blucybrb14de/gentoo-installguide)

<img src="https://nogithub.codeberg.page/badge.svg" alt="Please don't upload to GitHub"> [<img src="https://www.codeberg.org/Coding-Liberation.net/COPL/raw/branch/main/images/AC-COPL-badge.webp" alt="AC-COPL-badge.webp" width="110" height="20">](https://codeberg.org/Coding-Liberation.net/COPL)

<h2><em>📦 Featured</em> [Software]</h2>

<ul>
  <li><code>games-misc/grapejuice</code> - <a href="https://roblox.fandom.com/wiki/Roblox_on_Linux">Roblox-on-linux</a> installer utility</li>
  <li><code>app-admin/doas</code> - <a href="https://www.openbsd.org/">OpenBSD</a> alternative to sudo</li>
  <li><code>app-shells/shell-daddy</code> soft fork of <a href="https://github.com/sudofox/shell-mommy">shell-mommy</a> - for the gay and depraved</li>
  <li><code>net-im/betterdiscordctl</code> - utility for installing <a href="https://www.betterdiscord.app">betterdiscord</a> a discord rice/privacy enhancer</li>
  <li><code>app-backup/sanoid</code> - superior tool for automatically creating <a href="https://wiki.gentoo.org/wiki/ZFS">ZFS</a> snapshots</li>
  <li><code>app-eselect/eselect-llvm</code> - tool for managing <a href="https://wiki.gentoo.org/wiki/Project:LLVM">LLVM</a> toolchain, for clang</li>
  <li>All sources found in <a href="https://github.com/Szowisz/CachyOS-kernels">Cachyos-Kernels</a> overlay</li>
</ul>

<br>

## How to Enable the Overlay

---

<h3> <em>IMPORTANT</em> - If following <a href="https://codeberg.org/blucybrb14de/gentoo-installguide">blucybrb14de/gentoo-installguide</a> </h3> 
<p>The Overlay is <u>already</u> enabled.</p>

---

<br>

<h3>Using <a href="https://packages.gentoo.org/packages/app-eselect/eselect-repository">app-eselect/eselect-repository</a></h3>
<pre><code>eselect repository add cybrnet-overlay git https://codeberg.org/blucybrb14de/cybrnet-overlay</code></pre>

<br>

<h3>Installing in <code>/etc/portage/repos.conf/cybrnet-overlay.conf</code></h3>

<pre><code>[cybrnet-overlay]
location = /var/db/repos/cybrnet-overlay
sync-type = git
sync-uri = https://codeberg.org/blucybrb14de/cybrnet-overlay.git</code></pre>


<hr>

#### Changes Taken from:

- zzzb-gentoo
- nymphos
- loatchi
- Cachyos-Kernels

# Issues + Questions

- I will only respond to issues through <a href="https://codeberg.org/Codeberg/">Codeberg</a>
  - To report an issue for the configuration, please create <a href="https://docs.codeberg.org/getting-started/issue-tracking-basics">an issue</a>
- By reporting an issue, you are required to adhere to the Full <u><strong><a href="https://codeberg.org/Coding-Liberation/Manifesto/src/branch/main/CONTRIBUTING.md">CONTRIBUTING.md</a></strong></u>
    - This is a fork of the <a href="https://www.debian.org/code_of_conduct">Debian Code of Conduct</a>
      - Also note, the first clause & 3-6 of the <a href="https://www.funtoo.org/Wolf_Pack_Philosophy">Wolf-Pack philosphy</a></u> 
  - If at any point, you discover the solution to an issue, please <a href="https://www.funtoo.org/Wolf_Pack_Philosophy">Howl.</a><br>
- <strong>For Contact:</strong> <br><a href="https://proton.me/"><img src="https://codeberg.org/blucybrb14de/dotfiles/raw/branch/gentoo-hyprland/images/protonmail.webp"  style="vertical-align: middle" height="20" width="20"></a> blucybrb14de@coding-liberation.org

Mirrors

<a href="https://codeberg.org/blucybrb14de/cybrnet-overlay">
  <img src="https://codeberg.org/Codeberg/Design/raw/branch/main/logo/icon/svg/codeberg-logo_icon_blue.svg"
       alt="Codeberg"
       width="32">
</a>
<a href="https://gitlab.com/blucybrb14de/cybrnet-overlay">
  <img src="https://about.gitlab.com/images/press/press-kit-icon.svg"
       alt="Gitlab"
       width="32">
</a>

---

<a class="tooltip" href="https://codeberg.org/Coding-Liberation.net" data-content="Coding-Liberation.net">
<img class="ui avatar gt-vm" src="https://codeberg.org/repo-avatars/be28b9fa83042b94028bf1f1b28275eebb5926f957648b5faa5842732d512e20"
title="The Coding Liberation Front - for a better future" width="28" height="28" align="left"/></a>


<strong> LICENSE  &mdash;  Unique content of this project is licensed under the [COPL](https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/LICENSE) & [GFDL (GNU Free Documentation License)](https://codeberg.org/BLUcybrB14de/cybrnet-overlay/src/branch/main/COLICENSE)
</strong>

<a rel="license" href="https://www.gnu.org/licenses/fdl-1.3.en.html"><img alt="GNU Free Documentation License" style="border-width:0" height="125" src="https://static.wikia.nocookie.net/bttf/images/d/d1/Heckert_GNU_white.png/revision/latest?cb=20070222062917"/>

